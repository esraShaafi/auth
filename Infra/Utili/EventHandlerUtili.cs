﻿using Infra.DTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Utili
{
    public  class EventHandlerUtili : JwtBearerEvents
    {
        
        public async  override Task TokenValidated(TokenValidatedContext context)
        {
            await base.TokenValidated(context);
        }
        
        public override Task AuthenticationFailed(AuthenticationFailedContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            var result = new
            {
                Messages = "انتهت صلاحية الدخول",
                StatusCode = -2
            };
            var resultOperation = ResultOperationDTO<string>.CreateErrorOperation(messages: new string[] {
                result.Messages
            });

            var json = JsonConvert.SerializeObject(resultOperation);
            context.Response.WriteAsync(json);
            return Task.CompletedTask;
        }

        public  override Task Challenge(JwtBearerChallengeContext context)
        {
            string token;
            if (!TryRetrieveToken(context.Request, out token))
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                var result = new
                {
                    Messages = "لاتملك صلاحية الدخول",
                    StatusCode = -1
                };
                var resultOperation = ResultOperationDTO<string>.CreateErrorOperation(messages: new string[] {
                result.Messages
                });
                var json = JsonConvert.SerializeObject(resultOperation);
                context.Response.WriteAsync(json);
            }
             return Task.CompletedTask;
        }

        public override Task MessageReceived(MessageReceivedContext context)
        {
            return base.MessageReceived(context);
        }

        private  bool TryRetrieveToken(HttpRequest request, out string token)
        {
            token = null;
            Microsoft.Extensions.Primitives.StringValues authzHeaders;
            if (!request.Headers.TryGetValue("Authorization", out authzHeaders) || authzHeaders.Count() > 1)
            {
                return false;
            }
            var bearerToken = authzHeaders.ElementAt(0);
            token = bearerToken.StartsWith("Bearer ") ? bearerToken.Substring(7) : bearerToken;
            return true;
        }

    }
}
