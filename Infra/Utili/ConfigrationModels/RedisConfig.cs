﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Utili.ConfigrationModels
{
    public class RedisConfig
    {
        public string ConnecationString { get; set; }
        public double ExpiryRidesTime { get; set; }
    }
}
