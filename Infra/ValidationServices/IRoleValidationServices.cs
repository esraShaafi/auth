﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infra.ValidationServices
{
    public interface IRoleValidationServices
    {
        Task<bool> CheckRoleName(string roleName, long moduleID);
        Task<bool> CheckRoleNameDisplay(string roleNameDisplay, long moduleID);
        Task<bool> CheckRoleInInnerJoin(string userId);
        Task<bool> CheckIfRoleIsDeleted(string userId);
    }
}
