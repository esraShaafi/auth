﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infra.ValidationServices
{
    public interface IUserServicesValidation
    {
        Task<bool> CheckUserName(string userName);
        Task<bool> CheckEmail(string email);
    }
}
