﻿using Infra.Domain;
using Infra.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Services.IService
{
    public interface ILocationServices
    {
        Task<bool> InsertLocation(Locations location);
        Task<bool> UpdateLocation(Locations location);
        Task<bool> DeleteLocation(string locationId, bool isDelete);
        Task<bool> ActivateLocation(string locationId, bool isActive);
        Task<List<LocationDTO>> GetActiveLocation();
        Task<List<LocationDTO>> GetLocation(string locationId, bool isDelete);
        Task<IQueryable<Locations>> FindByLocation(Expression<Func<Locations, bool>> predicate);
    }
}
