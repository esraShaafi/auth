﻿using Infra.Domain;
using Infra.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Services.IService
{
    public interface IUserServices
    {

        Task<List<UsersDTO>> GetUsers();
        Task<IQueryable<Users>> FindUserBy(Expression<Func<Users, bool>> predicate);
        Task<bool> InsertUser(Users entity);
        Task<bool> UpdateUser(Users entity);
        Task<bool> DeleteUser(string userId, bool isDelete);
        Task<string[]> GetPermationUser(string userID);
        Task<List<UsersWithRoleDTO>> GetUsersWithRoles(UserClimesDTO currentUser, bool isActive);
        Task InsertUserModule(string userID, long moduleID);
        Task IsActiveAndDeActiveUserModule(string userID, long moduleID, bool IsActive, bool isPrimaryModule);
        Task<string> GetUserInModuleActive(string userId);
        Task<RestartPasswordDTO> RestartPassword(string userId);
        Task UpdateUserIsSendEmail(string userId);
        Task<List<UsersDTO>> GetUserWithDeActive(long moduleId, short userType);
        Task<bool> MoveUserInModule(string userID, long moduleId);
        Task<bool> ActivateUser(string userID, bool IsActive);
    }
}
