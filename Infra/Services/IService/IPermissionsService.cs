﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Infra.Domain;
using Infra.DTOs;
namespace Infra.Services.IService
{
    public interface IPermissionsService
    {
        Task<List<PermissionDTO>> getAllPermissions();
        Task<List<PermissionDTO>> GetPermissionIsArchiveOrDeArchive();
        Task<List<PermissionDTO>> GetActivePermission();
        Task<bool> ActivatePermission(string permissionId, bool isActive);
        Task<IQueryable<Permissions>> FindPermissionBy(Expression<Func<Permissions, bool>> predicate);

    }
}
