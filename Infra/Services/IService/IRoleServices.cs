﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Infra.Domain;
using Infra.DTOs;
namespace Infra.Services.IService
{
   public interface IRoleServices
    {
        Task<bool> InsertRole(Roles entity);
        Task<bool> UpdateRole(Roles entity);
        Task<bool> DeleteRole(string RoleId, bool isDeleted);
        Task<bool> ActivateRole(string RoleId, bool isActive);

        Task<List<RoleDTO>> GetActiveRoles(long moduleID, bool isActive);
        Task<List<RoleDTO>> GetAllRoles(long moduleID, bool isDeleted);
        Task<IQueryable<Roles>> FindRoleBy(Expression<Func<Roles, bool>> predicate);
        Task SaveChangesAsync();
    }

}
