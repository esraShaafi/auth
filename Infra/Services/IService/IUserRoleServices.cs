﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public interface IUserRoleServices
    {
        Task insertUserRole(string userId, List<string> roles);
        Task removeUserRole(string userId, long? moduleId);
    }
}
