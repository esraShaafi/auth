﻿using Infra.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Services.IService
{
    public interface IRolePermissionServices
    {
        Task InsertRolePermission(string roleId, List<string> permissions);
        Task RemoveRolePermission(string roleId);
        Task<List<PermissionDTO>> GetPermissionsInModule(long moduleId);
    }
}
