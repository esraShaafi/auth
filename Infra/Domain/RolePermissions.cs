﻿using System;
using System.Collections.Generic;

namespace Infra.Domain
{
    public partial class RolePermissions
    {
        public string PermissionId { get; set; }
        public string RoleId { get; set; }

        public virtual Permissions Permission { get; set; }
        public virtual Roles Role { get; set; }
    }
}
