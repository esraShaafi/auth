﻿using System;
using System.Collections.Generic;

namespace Infra.Domain
{
    public partial class Users
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string StateDescription { get; set; }
        public byte[] Photo { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public short? State { get; set; }
        public short? TypeUser { get; set; }
        public bool? IsSendEmail { get; set; }
        public DateTime? ExpiryDateEmail { get; set; }
        public short? TransactionProccess { get; set; }
        public string RoleId { get; set; }
        public long ModuleId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual Modules Module { get; set; }
        public virtual Roles Role { get; set; }
    }
}
