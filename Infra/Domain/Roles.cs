﻿using System;
using System.Collections.Generic;

namespace Infra.Domain
{
    public partial class Roles
    {
        public Roles()
        {
            RolePermissions = new HashSet<RolePermissions>();
            Users = new HashSet<Users>();
        }

        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public long? ModuleId { get; set; }
        public string RoleNameDisplay { get; set; }
        public bool? IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual Modules Module { get; set; }
        public virtual ICollection<RolePermissions> RolePermissions { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
