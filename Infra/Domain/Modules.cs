﻿using System;
using System.Collections.Generic;

namespace Infra.Domain
{
    public partial class Modules
    {
        public Modules()
        {
            Permissions = new HashSet<Permissions>();
            Roles = new HashSet<Roles>();
            Users = new HashSet<Users>();
        }

        public long ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Permissions> Permissions { get; set; }
        public virtual ICollection<Roles> Roles { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
