﻿using System;
using System.Collections.Generic;

namespace Infra.Domain
{
    public partial class Permissions
    {
        public Permissions()
        {
            RolePermissions = new HashSet<RolePermissions>();
        }

        public string PermissionId { get; set; }
        public string PermissionName { get; set; }
        public long? ModuleId { get; set; }
        public string PermissionDispaly { get; set; }
        public bool IsActive { get; set; }

        public virtual Modules Module { get; set; }
        public virtual ICollection<RolePermissions> RolePermissions { get; set; }
    }
}
