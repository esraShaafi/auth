﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
    public class UsersWithRoleDTO : UsersDTO
    {
        public string ModuleName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsPrimaryModule { get; set; }
        public List<RoleDTO> roles { get; set; }
    }
}
