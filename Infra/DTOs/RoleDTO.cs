﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
   public class RoleDTO
    {
        public object permissions;

        public string RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleNameDisplay { get; set; }
        public List<PermissionDTO> RolePermissions { get; set; }
    }
}
