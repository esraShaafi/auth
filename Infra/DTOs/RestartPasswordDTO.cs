﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
    public class RestartPasswordDTO
    {
        public string NewPasswordHash { get; set; }
        public string UserEmail { get; set; }
    }
}
