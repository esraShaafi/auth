﻿using Infra.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
    public class OAuthUserDTO
    {
        public string AccessToken { get; set; }
        public string IdRefreshToken { get; set; }
        public string[] UserPermissions { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Photo { get; set; }
        public string UserEmail { get; set; }
        //public List<RoleDTO> UserRoleIsAdmin { get; set; }
        public Modules Module  { get; set; }
    }
}
