﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs.Routers
{
    public class ManagmentControllerRouter
    {
        public const string ControllerNameRoute = "api/Managment";
        public const string InsertUser = "InsertUser";
        public const string GetUsers = "GetUsers";
        public const string GetUsersWithRole = "GetUsersWithRole";
        public const string UpdateUser = "UpdateUser";
        public const string IsActiveOrDeActiveUser = "IsActiveOrDeActiveUser";
        public const string RestartPassword = "RestartPassword";
        public const string Activate = "ActivateUser";
        public const string Delete = "DeleteUser";


    }
}
