﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs.Routers
{
    public static class LoginControllerRouter
    {
        public const string AuthUser = "AuthUser";
        public const string RefreshToken = "RefreshToken";
        public const string ControllerNameRoute = "api/LoginUser";
    }
}
