﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs.Routers
{
   public class RoleControllerRouter
    {
        public const string ControllerNameRoute = "api/Roles";
        public const string InserRole = "InserRole";
        public const string UpdateRole = "UpdateRole";
        public const string IsActiveOrDeActiveRole = "IsActiveOrDeActiveRole";
        public const string GetRoles = "GetRoles";
        public const string GetActiveRoles = "GetActiveRoles";
        
        public const string GetModulePermissions = "GetModulePermissions";
        public const string DeleteRole = "DeleteRole";

    }
}
