﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs.Routers
{
    public class PermissionsControllerRouter
    {
        public const string ControllerNameRoute = "api/Permissions";
        public const string GetPermissions = "GetPermissions";
        public const string GetActivePermissions = "GetActivePermissions";
        public const string GetAllPermissions = "GetAllPermissions";
        public const string Activate = "Activate";
    }
}
