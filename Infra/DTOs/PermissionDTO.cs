﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
   public class PermissionDTO
    {
        public string PermissionID { get; set; }
        public string PermissionName { get; set; }
        public string PermissionDispaly { get; set; }
        public bool isActive { get; set; }
    }
}
