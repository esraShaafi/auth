﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
    public class UserClimesDTO
    {
        public string UserID { get; set; }
        public short? TypeUser { get; set; }
        public long ModuleID { get; set; }
        public string UserName { get; set; }
    }
}
