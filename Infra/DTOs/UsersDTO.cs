﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
    public class UsersDTO
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        // public string Photo { get; set; }
        public string PhoneNumber { get; set; }
        public string StateDescription { get; set; }

    }
}
