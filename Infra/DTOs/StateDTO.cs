﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.DTOs
{
    public enum StateDTO
    {
        Active,
        Delete
    }

    public enum StateContact
    {
        Email,
        PhoneNum,
        FAX
    }

    public enum StateTransfer
    {
        HF,
        Unit
    }

    public enum StateProductGroup
    {
        Essential,
        Complementary
    }


    public enum StateInventory
    {
        IsProccess,
        NotComblete,
        IsCombleted
    }
}
