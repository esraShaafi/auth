﻿using Infra.Domain;
using Infra.DTOs;
using Infra.Services.IService;
using Infra.Utili;
using Infra.Utili.ConfigrationModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace OAuthUser.API.OAuth
{
    public  class Auth
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IOptions<AppSettingsConfig> _settings;
        public Auth(IHttpContextAccessor httpContextAccessor, IOptions<AppSettingsConfig> settings)
        {
            _httpContextAccessor = httpContextAccessor;
            _settings = settings;
        }
        public async Task<string> SingIn(Users user, string[] permissions)
        {
            await Task.FromResult(true);
            DateTime expired = DateTime.Now.AddHours(_settings.Value.AddHourExpired);
            DateTime issuedAt = DateTime.Now;

            var claims = this.addClaimsIdentity(user, expired, permissions);
            return await CreateToken(issuedAt, expired, claims);

        }

        private ClaimsIdentity addClaimsIdentity(Users user, DateTime expired, string[] permissions)
        {
            var isPrimaryModule = user.Module.ModuleId;
            ClaimsIdentity identityClaims = new ClaimsIdentity();
            identityClaims.AddClaim(new Claim(ClaimTypes.Sid, user.UserId));
            identityClaims.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identityClaims.AddClaim(new Claim(ClaimTypes.Hash, Guid.NewGuid().ToString()));
            identityClaims.AddClaim(new Claim(ClaimTypes.Expired, expired.ToString()));
            identityClaims.AddClaim(new Claim(ClaimTypes.System, isPrimaryModule.ToString()));
            identityClaims.AddClaim(new Claim("TypeUser", user.TypeUser.ToString()));


            foreach (var itemPermissions in permissions)
            {
                identityClaims.AddClaim(new Claim(ClaimTypes.Role, itemPermissions));
            }

            return identityClaims;

        }
        public async Task<string> refreshToken(string userID)
        {
            //param IdTokenRefresh And UserID 
            // يتم التحقق من الوقت التوكن كان فات 5دقائق لاتتم عملية تحديث التوكن

            await Task.FromResult(true);
            DateTime issuedAt = DateTime.Now;
            var user = _httpContextAccessor.HttpContext.User as ClaimsPrincipal;
            var identity = user.Identity as ClaimsIdentity;
            var claim = user.Claims.SingleOrDefault(s => s.Value == userID);
            var expiredDate = identity.FindFirst(ClaimTypes.Expired);
            identity.RemoveClaim(expiredDate);
            var expired = DateTime.Now.AddHours(_settings.Value.AddHourExpired);
            identity.AddClaim(new Claim(ClaimTypes.Expired, expired.ToString()));
            return await CreateToken(issuedAt, expired, identity);

            //Update in Caching Token and UserId(key) and TokenRefreshId
        }

        private async Task<string> CreateToken(DateTime issuedAt, DateTime expired, ClaimsIdentity identityClaims)
        {
            await Task.FromResult(true);
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(_settings.Value.Secret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            TokenValidationParameters validationParameters = new TokenValidationParameters()
            {
                ValidAudience = _settings.Value.ValidAudience,
                ValidIssuer = _settings.Value.ValidIssuer,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                LifetimeValidator = HelperUtili.LifetimeValidator,
                IssuerSigningKey = securityKey,
            };
            var createToken =
               (JwtSecurityToken)
                   tokenHandler.CreateJwtSecurityToken(issuer: "", audience: "",
                       subject: identityClaims, notBefore: issuedAt, expires: expired, signingCredentials: signingCredentials);

            return tokenHandler.WriteToken(createToken);
        }

    }
}
