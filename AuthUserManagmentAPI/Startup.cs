using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FilterAttributeWebAPI.Common;
using FluentValidation.AspNetCore;
using Infra.Domain;
using Infra.Repository;
using Infra.Services;
//using Infra.Services.ISendEmail;
using Infra.Services.IService;
using Infra.Utili;
using Infra.Utili.ConfigrationModels;
using Infra.Utili.Filters;
//using Infra.ValidationServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using OAuthUser.API.BackgroundServices;
using OAuthUser.API.Mapping;
using OAuthUser.API.OAuth;
using Repository.Repository;
using Microsoft.OpenApi.Models;
using UnitOfWork.Work;
using Infra;
using Services.Services;
using Infra.ValidationServices;
using Services.ValidationServices;

namespace OAuthUser.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
           
            services.AddOptions();
            services.AddAutoMapper(typeof(Startup));
            //services.AddHostedService<SendEmailBackgroundService>();
            services.AddCors(opt => opt.AddPolicy("CorsPolicy",
               builder =>
               {
                   builder.WithOrigins("*")
                       .AllowAnyHeader().
                        AllowAnyOrigin()
                       .AllowAnyMethod();
               }));

            services.AddSwaggerGen(c => c.SwaggerDoc(name: "v1", new OpenApiInfo { Title = "My API", Version = "V1" }));

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettingsConfig>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettingsConfig>();


            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(appSettings.Secret));
            
            services.AddAuthentication("OAuth").AddJwtBearer("OAuth", config =>
            {
                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    LifetimeValidator = HelperUtili.LifetimeValidator,
                    IssuerSigningKey = securityKey,
                    SaveSigninToken=true,
                };

                config.EventsType = typeof(EventHandlerUtili);
                config.SaveToken = true;
            });
            services.AddControllers(config =>
            {
                config.Filters.Add<ExceptionHandlerUtili >();
                config.Filters.Add<ParametorModelFilter>();
            }
            ).AddFluentValidation(config=> {
                config.RegisterValidatorsFromAssemblyContaining<Startup>();
            });
            DI(services);
            
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CorsPolicy");
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json","Test API V1"));
        }

       
        private void DI(IServiceCollection services)
        {
            var con = @"Server=.;Database=AuthUserDB;user id=sa;password=123456";
            services.AddDbContext<AuthUserDBContext>(options => options.UseSqlServer(con));
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork<AuthUserDBContext>));
            services.TryAddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddScoped<EventHandlerUtili>();
            services.TryAddScoped<Auth>();
            services.TryAddScoped<HelperUtili>();
            services.AddScoped<IPermissionsService, PermissionsService>();
            services.AddScoped<IRoleServices, RoleService>();
            services.AddScoped<IRolePermissionServices, RolePermissionServices>();
            services.TryAddScoped<IUserServices, UserServices>();
            services.AddScoped<IRoleValidationServices, RoleValidationServices>();
            services.AddScoped<IUserServicesValidation, UserServicesValidation>();

        }
    }
}
