﻿using Infra.DTOs;
using Infra.ValidationServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthUserManagmentAPI.Filters.role
{
    public class CheckIfRoleIsDeletedFilterAttribute: ActionFilterAttribute
    {
        private readonly IRoleValidationServices _roleValidationServices;
        public CheckIfRoleIsDeletedFilterAttribute(IRoleValidationServices roleValidationServices)
        {
            _roleValidationServices = roleValidationServices;
        }


        public async override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var paramAreaId = context.ActionArguments.TryGetValue("roleId", out var _roleId);


            if (!paramAreaId)
            {
                context.Result = new BadRequestObjectResult(ResultOperationDTO<bool>.
                  CreateErrorOperation(messages: new string[] { $"لم يتم ارسال رقم التعريف الدور " }));
                return;
            }


            if (_roleId is string roleId)
            {
                if (await _roleValidationServices.CheckIfRoleIsDeleted(roleId))
                {
                    context.Result = new BadRequestObjectResult(ResultOperationDTO<bool>.
                       CreateErrorOperation(messages: new string[] { $"بيانات الدور  تم ارشفتها من قبل مستخدم أخر  " }));
                    return;
                }

            }
            await base.OnActionExecutionAsync(context, next);
        }
    }
}
