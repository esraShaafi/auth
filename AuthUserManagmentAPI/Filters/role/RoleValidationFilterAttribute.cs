﻿using AuthUserManagmentAPI.Models;
using Infra.DTOs;
using Infra.Services.IService;
using Infra.Utili;
using Infra.ValidationServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthUserManagmentAPI.Filters
{
    public class RoleValidationFilterAttribute: ActionFilterAttribute
    {
        private readonly IRoleServices _roleServices;
        private readonly IRoleValidationServices _roleValidationServices;
        private readonly HelperUtili _helper;
        public RoleValidationFilterAttribute(IRoleServices roleServices, IRoleValidationServices roleValidationServices, HelperUtili helper)
        {
            _roleServices = roleServices;
            _roleValidationServices = roleValidationServices;
            _helper = helper;
        }

        public async override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var param = context.ActionArguments.TryGetValue("roleModel", out var _roleModel);

            if (param && _roleModel is RoleModel roleModel)
            {
                //roleModel.CreatedBy = _helper.GetCurrentUser().UserID;
                //roleModel.CreatorUserName = _helper.GetCurrentUser().UserName;

                if (await RefactorCode(context, roleModel.ModuleId, roleNameDisplay: roleModel.RoleNameDisplay ))
                {
                    return;
                }
                if (await RefactorCode(context, roleModel.ModuleId, roleName: roleModel.RoleName))
                {
                    return;
                }
            }

            var paramUpdateRole = context.ActionArguments.TryGetValue("roleModelUpdate", out var _roleModelUpdate);
            if (paramUpdateRole && _roleModelUpdate is RoleModel roleModelUpdate)
            {
                var result = await (await _roleServices.FindRoleBy(role => role.RoleId == roleModelUpdate.RoleId && role.IsDeleted == false)).SingleOrDefaultAsync();

                if (result == null)
                {
                    context.Result = new BadRequestObjectResult(ResultOperationDTO<bool>.
                      CreateErrorOperation(messages: new string[] { $" بيانات الدور تم ارشفتها من قبل مستخدم أخر " }));
                    return;
                }
                if (result.RoleNameDisplay.ToLower() != roleModelUpdate.RoleNameDisplay.ToLower() && await RefactorCode(context, roleModelUpdate.ModuleId, roleNameDisplay: roleModelUpdate.RoleNameDisplay))
                {
                    return;
                }
                if (result.RoleName.ToLower() != roleModelUpdate.RoleName.ToLower() && await RefactorCode(context, roleModelUpdate.ModuleId, roleNameDisplay: roleModelUpdate.RoleNameDisplay))
                {
                    return;
                }

            }
            await base.OnActionExecutionAsync(context, next);
        }
        private async Task<bool> RefactorCode(ActionExecutingContext context, long moduleID, string roleName = "", string roleNameDisplay = "")
        {

            if (!string.IsNullOrEmpty(roleName) && await _roleValidationServices.CheckRoleNameDisplay(roleNameDisplay, moduleID))
            {
                context.Result = new BadRequestObjectResult(ResultOperationDTO<bool>.
                    CreateErrorOperation(messages: new string[] { $" اسم الدور موجود مسبقا" }));
                return true;
            }

            if (!string.IsNullOrEmpty(roleName) && await _roleValidationServices.CheckRoleName(roleName,moduleID))
            {
                context.Result = new BadRequestObjectResult(ResultOperationDTO<bool>.
                    CreateErrorOperation(messages: new string[] { $" اسم الدور بلغة الانجليزية موجود مسبقا" }));
                return true;
            }
            return false;
        }

    }
}
