﻿using AuthUserManagmentAPI.Models;
using Infra.DTOs;
using Infra.Services.IService;
using Infra.Utili;
using Infra.ValidationServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthUserManagmentAPI.Filters
{
    public class UserValidationFilterAttribute: ActionFilterAttribute
    {
        private readonly IUserServicesValidation _userServicesValidation;
        private readonly IUserServices _userServices;
        private readonly HelperUtili _helper;

        public UserValidationFilterAttribute(IUserServicesValidation userServicesValidation, IUserServices userServices,
            HelperUtili helper)
        {
            _userServicesValidation = userServicesValidation;
            _userServices = userServices;
            _helper = helper;
        }

        public async override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var param = context.ActionArguments.TryGetValue("addUserModel", out var users);
            if (param && users is UserModel user)
            {
                if (await RefactorCode(context, email: user.Email))
                {
                    return;
                }

                if (await RefactorCode(context, userName: user.UserName))
                {
                    return;
                }

            }

            var updateUserParam = context.ActionArguments.TryGetValue("updateUserModel", out var updateUser);
            if (updateUserParam && updateUser is UpdateUserModel updateUserObj)
            {
                if (updateUserObj.ModuleId == 0)
                    updateUserObj.ModuleId = _helper.GetCurrentUser().ModuleID;

                var resultUser = await (await _userServices.
                    FindUserBy(user => user.UserId == updateUserObj.UserId )).
                    SingleOrDefaultAsync();

                if (resultUser == null)
                {
                    context.Result = new OkObjectResult(ResultOperationDTO<bool>.
                        CreateErrorOperation(messages: new string[] { $" لايمكن تعديل بيانات هذا المستخدم  {updateUserObj.UserName}" }));
                    return;
                }
                if (resultUser.Email != updateUserObj.Email && await RefactorCode(context, email: updateUserObj.Email))
                {
                    return;
                }

                if (resultUser.UserName != updateUserObj.UserName && await RefactorCode(context, userName: updateUserObj.UserName))
                {
                    return;
                }
            }


            await base.OnActionExecutionAsync(context, next);
        }

        private async Task<bool> RefactorCode(ActionExecutingContext context, string email = "", string userName = "")
        {
            if (!string.IsNullOrEmpty(email) && await _userServicesValidation.CheckEmail(email))
            {
                context.Result = new OkObjectResult(ResultOperationDTO<object>.CreateErrorOperation(messages:
                    new string[] { "الاميل موجود مسبقا" }));
                return true;
            }

            if (!string.IsNullOrEmpty(userName) && await _userServicesValidation.CheckUserName(userName))
            {
                context.Result = new OkObjectResult(ResultOperationDTO<object>.CreateErrorOperation(messages:
                   new string[] { " الاسم موجود مسبقا" }));
                return true;
            }
            return false;
        }
    }
}
