﻿using AuthUserManagmentAPI.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthUserManagmentAPI.ValidationModel
{
    public class RoleValidationModel:AbstractValidator<RoleModel>
    {
        public RoleValidationModel()
        {
            RuleFor(role => role.RoleName).NotEmpty().WithMessage("يجب ادخال اسم الدور");
            RuleFor(role => role.RoleNameDisplay).NotEmpty().WithMessage("يجب ادخال اسم الدور");
        }
    }
}
