﻿using AuthUserManagmentAPI.Models;
using AuthUserManagmentAPI.Models.RolesModel;
using AutoMapper;
using Infra.Domain;
using Infra.DTOs;
using Infra.Utili;
//using OAuthUser.API.Models.RolesModel;
//using OAuthUser.API.Models.UsersModel;
using System;

namespace OAuthUser.API.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RoleModel, Roles>().
              ForMember(dest => dest.RoleId, opt =>
                    opt.MapFrom(src => string.IsNullOrEmpty(src.RoleId) ? Guid.NewGuid().ToString() : src.RoleId)).
              ForMember(dest => dest.IsActive, opt => opt.Condition(src => string.IsNullOrEmpty(src.RoleId))).
              ForMember(dest => dest.IsDeleted, opt => opt.Condition(src => string.IsNullOrEmpty(src.RoleId))).
             
              ForMember(dest => dest.CreatedOn, opt =>
                    opt.Condition(src => string.IsNullOrEmpty(src.RoleId)))
             ;

            CreateMap<UserModel, Users>().ForMember(dest => dest.State, opt => opt.MapFrom(src => (short)StateDTO.Active)).
             ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => DateTime.Now)).
             ForMember(dest => dest.UserId, opt => opt.MapFrom(src => Guid.NewGuid().ToString())).
             ForMember(dest => dest.ExpiryDateEmail, opt => opt.MapFrom(src => DateTime.Now.AddHours(1))).
             ForMember(dest => dest.IsSendEmail, opt => opt.MapFrom(src => true))
             ;
               CreateMap<UpdateUserModel, Users>().ForMember(dest => dest.State, opt => opt.MapFrom(src => (short)StateDTO.Active)).
             ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => DateTime.Now)).
            // ForMember(dest => dest.UserId, opt => opt.MapFrom(src => Guid.NewGuid().ToString())).
             ForMember(dest => dest.ExpiryDateEmail, opt => opt.MapFrom(src => DateTime.Now.AddHours(1))).
             ForMember(dest => dest.IsSendEmail, opt => opt.MapFrom(src => true))
             ;




        }
    }
}
