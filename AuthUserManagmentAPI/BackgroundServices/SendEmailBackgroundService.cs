﻿using Infra.Services.BackgroundServices;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OAuthUser.API.BackgroundServices
{
    public class SendEmailBackgroundService : BackgroundService
    {
        private Timer _timer;
        
        private readonly IServiceProvider _provider;
        public SendEmailBackgroundService(IServiceProvider serviceProvider)
        {
            _provider = serviceProvider;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                using (IServiceScope scope = _provider.CreateScope())
                {
                    var _sendEmailBackgroundService = scope.ServiceProvider.GetRequiredService<ISendEmailBackgroundService>();
                    try
                    {
                        _timer = new Timer(async (e) => await _sendEmailBackgroundService.SendEmail(), null, TimeSpan.Zero, TimeSpan.FromMinutes(2));
                    }
                    catch (Exception)
                    {

                   
                    }
                  
                }
            }
            catch (Exception)
            {
                
            }
            await Task.CompletedTask;
        }
        public async override Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            await Task.CompletedTask;
        }
    }
}
