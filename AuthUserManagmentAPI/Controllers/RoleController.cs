﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Infra.DTOs.Routers;
using Infra.Services.IService;
using Infra;
using Infra.DTOs;
using Infra.Domain;
using AuthUserManagmentAPI.Models.RolesModel;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using AuthUserManagmentAPI.Models;
using AuthUserManagmentAPI.Filters;
using AuthUserManagmentAPI.Filters.role;

namespace AuthUserManagmentAPI.Controllers
{
    [Route(RoleControllerRouter.ControllerNameRoute)]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleServices _roleService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IRolePermissionServices _rolePermissionServices;
        public RoleController(IRoleServices roleService, IUnitOfWork unitOfWork ,
            IRolePermissionServices rolePermissionServices , IMapper mapper)
        {
            _rolePermissionServices = rolePermissionServices;
            _mapper = mapper;
            _roleService = roleService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet(RoleControllerRouter.GetActiveRoles)]
        public async Task<ActionResult<ResultOperationDTO<List<RoleDTO>>>> GetActiveRoles(long moduleId = 0, bool isActive = true)
        {
            return ResultOperationDTO<List<RoleDTO>>.CreateSuccsessOperation(await _roleService.GetActiveRoles(moduleId, isActive));
            //var result = await _roleService.GetRoles(isArchive);
            //return ResultOperationDTO<List<RoleDTO>>.CreateSuccsessOperation(result);
        }
        [HttpGet(RoleControllerRouter.GetRoles)]
        public async Task<ActionResult<ResultOperationDTO<List<RoleDTO>>>> GetRoles(long moduleId = 0, bool isDeleted = true)
        {
            return ResultOperationDTO<List<RoleDTO>>.CreateSuccsessOperation(await _roleService.GetAllRoles(moduleId, isDeleted));
       
        }

        [HttpPost(RoleControllerRouter.InserRole)]
        [TypeFilter(typeof(RoleValidationFilterAttribute))]
        public async Task<ActionResult<ResultOperationDTO<RoleModel>>> InsertRole(RoleModel roleModel)
        {
            var result = _mapper.Map<Roles>(roleModel);
            roleModel.RoleId = result.RoleId;
            await _roleService.InsertRole(result);
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<RoleModel>.
                CreateSuccsessOperation(roleModel, message: new string[] { "تمت عملية بنجاح" });
        }

        [HttpDelete(RoleControllerRouter.DeleteRole)]
        [TypeFilter(typeof(CheckIfRoleInInnerJoinFilterAttribute))]

        public async Task<ActionResult<ResultOperationDTO<bool>>> DeleteRole(string roleId, bool isDelete)
        {
            var result = await _roleService.DeleteRole(roleId, isDelete);
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<bool>.CreateSuccsessOperation(result, message: new string[] { "تمت عملية بنجاح" });

        }
        [HttpPut(PermissionsControllerRouter.Activate)]
        [TypeFilter(typeof(CheckIfRoleIsDeletedFilterAttribute))]

        // [TypeFilter(typeof(CheckIfAreaIsDeletedFilterAttribute))]
        public async Task<ActionResult<ResultOperationDTO<bool>>> Activate(string roleId, bool isActive)
        {
            await _roleService.ActivateRole(roleId, isActive);
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<bool>.CreateSuccsessOperation(true, message: new string[] { "تمت عملية بنجاح" });
        }
       // [Authorize]
        [HttpGet(RoleControllerRouter.GetModulePermissions)]
        public async Task<ActionResult<ResultOperationDTO<List<PermissionDTO>>>> GetModulePermissions(long moduleId = 0)
        {
            // if (moduleId == 0)
            //    moduleId = _helper.GetCurrentUser().ModuleID;
            var resulte =  await _rolePermissionServices.GetPermissionsInModule(moduleId);
            return ResultOperationDTO<List<PermissionDTO>>.
                CreateSuccsessOperation(resulte);
        }

       // [Authorize]
        [HttpPut(RoleControllerRouter.UpdateRole)]
        [TypeFilter(typeof(RoleValidationFilterAttribute))]
        public async Task<ActionResult<ResultOperationDTO<RoleModel>>> UpdateRole(RoleModel roleModelUpdate)
        {
            var result = await (await _roleService.FindRoleBy(role => role.RoleId == roleModelUpdate.RoleId)).SingleOrDefaultAsync();
            _mapper.Map(roleModelUpdate, result);
            await _roleService.UpdateRole(result);
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<RoleModel>.CreateSuccsessOperation(roleModelUpdate, message: new string[] { "تمت عملية بنجاح" });
        }

    }
}