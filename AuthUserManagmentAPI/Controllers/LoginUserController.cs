﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AuthUserManagmentAPI.Models;
using AutoMapper;
using Infra.DTOs;
using Infra.DTOs.Routers;
using Infra.Services.IService;
using Infra.Utili;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OAuthUser.API.OAuth;

namespace AuthUserManagmentAPI.Controllers
{
   
       // [Authorize]
        [Route(LoginControllerRouter.ControllerNameRoute)]
        public class LoginUserController : ControllerBase
         {
            private readonly IUserServices _userServices;
            private readonly Auth _auth;
            private readonly HelperUtili _helper;
            private readonly IMapper _mapper;
            public LoginUserController(IUserServices userServices, Auth auth, HelperUtili helper, IMapper mapper)
            {
                _userServices = userServices;
                _auth = auth;
                _helper = helper;
                _mapper = mapper;
            }

          //  [AllowAnonymous]
            [HttpPost(LoginControllerRouter.AuthUser)]
            public async Task<ActionResult<ResultOperationDTO<OAuthUserDTO>>> AuthUser( UserAuthModel userAuthModel)
            {

                var username = userAuthModel.UserName;
                var password = userAuthModel.PasswordHash;
                var result = (await _userServices.FindUserBy(user => (user.Email == userAuthModel.UserName || user.UserName == userAuthModel.UserName && user.IsActive==true) ));
                
                if (result.SingleOrDefault() == null)
                {
                    return ResultOperationDTO<OAuthUserDTO>.CreateErrorOperation(messages: new string[] { "تأكد من إسم المستخدم او البريد الالكتروني" });
                }
                var r = result.SingleOrDefault();
                if (result.SingleOrDefault() != null 
                //&& !_helper.VerifyHash(userAuthModel.PasswordHash, result.SingleOrDefault().PasswordHash)
                )
                {
                   // return ResultOperationDTO<OAuthUserDTO>.CreateErrorOperation(messages: new string[] { "تأكد من كلمة المرور " });
                }



                var userWithPermissions = await _userServices.GetPermationUser(result.SingleOrDefault().UserId);


                var access_Token = await _auth.SingIn(result.SingleOrDefault(), userWithPermissions);

                return ResultOperationDTO<OAuthUserDTO>.CreateSuccsessOperation(result: new OAuthUserDTO
                {
                    AccessToken = access_Token,
                    IdRefreshToken = Guid.NewGuid().ToString(),
                    UserPermissions = userWithPermissions,
                    UserEmail = result.SingleOrDefault().Email,
                    UserName = result.SingleOrDefault().UserName,
                    UserID = result.SingleOrDefault().UserId,
                    Module = result.SingleOrDefault().Module
                });
            }

            [Authorize]
            [HttpGet(LoginControllerRouter.RefreshToken)]
            public async Task<ActionResult<ResultOperationDTO<OAuthUserDTO>>> refreshToken()
            {
                var userCliams = _helper.GetCurrentUser();
                var claims = HttpContext.User.Claims.ToList();
                var dateTimeExp = Convert.ToDateTime(HttpContext.User.FindFirst(ClaimTypes.Expired).Value);
                var refreshToken = await _auth.refreshToken(userCliams.UserID);
                return ResultOperationDTO<OAuthUserDTO>.CreateSuccsessOperation(result: new OAuthUserDTO
                {
                    AccessToken = refreshToken,
                    IdRefreshToken = Guid.NewGuid().ToString()
                });
            }



        }
    }