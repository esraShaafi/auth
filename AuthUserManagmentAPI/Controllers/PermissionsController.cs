﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infra.DTOs.Routers;
using Infra.DTOs;
using Infra.Domain;
using Infra.Services.IService;
using Infra;

namespace AuthUserManagmentAPI.Controllers
{
    [Route(PermissionsControllerRouter.ControllerNameRoute)]

    public class PermissionsController : ControllerBase
    {
        private readonly IPermissionsService _permissionsService;
        private readonly IUnitOfWork _unitOfWork;

        public PermissionsController(IPermissionsService permissionsService, IUnitOfWork unitOfWork)
        {
            _permissionsService = permissionsService;
            _unitOfWork = unitOfWork;
        }
        // [Authorize]
        [HttpGet(PermissionsControllerRouter.GetAllPermissions)]
        public async Task<ActionResult<ResultOperationDTO<List<PermissionDTO>>>> GetPermissions()
        {
            var result = await _permissionsService.getAllPermissions();
            return ResultOperationDTO<List<PermissionDTO>>.CreateSuccsessOperation(result);
        }
        [HttpPut(PermissionsControllerRouter.Activate)]
        // [TypeFilter(typeof(CheckIfAreaIsDeletedFilterAttribute))]
        public async Task<ActionResult<ResultOperationDTO<bool>>> Activate(string permissionId, bool isActive)
        {
            await _permissionsService.ActivatePermission(permissionId, isActive);
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<bool>.CreateSuccsessOperation(true, message: new string[] { "تمت عملية بنجاح" });
        }
        [HttpGet(PermissionsControllerRouter.GetActivePermissions)]
        public async Task<ActionResult<ResultOperationDTO<List<PermissionDTO>>>> GetActivePermissions()
        {
            var result = await _permissionsService.GetActivePermission();
            return ResultOperationDTO<List<PermissionDTO>>.CreateSuccsessOperation(result);
        }
    }
}