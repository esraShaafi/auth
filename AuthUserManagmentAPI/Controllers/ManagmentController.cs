﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthUserManagmentAPI.Filters;
using AuthUserManagmentAPI.Filters.role;
using AuthUserManagmentAPI.Models;
using AutoMapper;
using Infra;
using Infra.Domain;
using Infra.DTOs;
using Infra.DTOs.Routers;
using Infra.Services.IService;
using Infra.Utili;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuthUserManagmentAPI.Controllers
{
    [Route(ManagmentControllerRouter.ControllerNameRoute)]
    [ApiController]
    public class ManagmentController : ControllerBase
    {
        private readonly IUserServices _userService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly HelperUtili _helper;
        public ManagmentController(IUserServices userService, IUnitOfWork unitOfWork,
             IMapper mapper, HelperUtili helper)
        {
            _mapper = mapper;
            _userService = userService;
            _unitOfWork = unitOfWork;
            _helper = helper;
        }

        [HttpGet(ManagmentControllerRouter.GetUsers)]
        public async Task<ActionResult<ResultOperationDTO<List<UsersDTO>>>> GetUsers()
        {
            var result = await _userService.GetUsers();
            return ResultOperationDTO<List<UsersDTO>>.CreateSuccsessOperation(result);
        }
        //[HttpGet(ManagmentControllerRouter.GetUsersWithRole)]
        //public async Task<ActionResult<ResultOperationDTO<List<UsersWithRoleDTO>>>> GetUsersWithRole(long module = 0, bool isActive = true)
        //{
        //    var currentUser = _helper.GetCurrentUser();
        //    if (module > 0)
        //        currentUser.ModuleID = module;

        //    var result = await _userService.GetUsersWithRoles( currentUser, isActive);
        //    return ResultOperationDTO<List<UsersWithRoleDTO>>.CreateSuccsessOperation(result);
        //}

        [HttpPost(ManagmentControllerRouter.InsertUser)]
        [TypeFilter(typeof(UserValidationFilterAttribute))]

        public async Task<ActionResult<ResultOperationDTO<bool>>> AddUser(UserModel addUserModel)
        {
            string message = $"تم ارسال كلمة المرور الي بريد الالكتروني {addUserModel.Email} ";

            var user = _mapper.Map<Users>(addUserModel);

            user.CreatedBy = "esra";
                //_helper.GetCurrentUser().UserID;
            user.PasswordHash = _helper.Hash(addUserModel.PasswordHash);

          //  if (addUserModel.ModuleId == 0)
              //  addUserModel.ModuleId = _helper.GetCurrentUser().ModuleID;

            await _userService.InsertUser(user);
            //  await _userService.InsertUserModule(user.UserId, addUserModel.ModuleId);

            //   await _userRoleServices.insertUserRole(user.UserId, addUserModel.Roles);

            //if (!await _sendEmail.sendEmailAsync(user.Email, addUserModel.PasswordHash))
            //{
            //    user.IsSendEmail = false;
            //    user.TransactionProccess = (short)TransactionState.Proccess;
            //    message = "لم يتم ارسال كلمة المرور الي البريد الإلكتروني ! سيتم ارساله لاحقا";
            //}
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<bool>.CreateSuccsessOperation(true, message: new string[] { message });
        }
        [TypeFilter(typeof(UserValidationFilterAttribute))]
        [HttpPut(ManagmentControllerRouter.UpdateUser)]
        public async Task<ActionResult<ResultOperationDTO<bool>>> UpdateUser(UpdateUserModel updateUserModel)
        {
            //if (updateUserModel.ModuleId == 0)
            //    updateUserModel.ModuleId = _helper.GetCurrentUser().ModuleID;

            var result = await (await _userService.FindUserBy(user => user.UserId == updateUserModel.UserId )).SingleOrDefaultAsync();

            _mapper.Map(updateUserModel, result);

            //await _userRoleServices.removeUserRole(result.UserId, updateUserModel.ModuleId);
           // await _userRoleServices.insertUserRole(result.UserId, updateUserModel.Roles);

            await _userService.UpdateUser(result);
            await _unitOfWork.SaveChangeAsync();

            return ResultOperationDTO<bool>.CreateSuccsessOperation(true);
        }
        [HttpPut(ManagmentControllerRouter.Activate)]
       // [TypeFilter(typeof(CheckIfAreaIsDeletedFilterAttribute))]
        public async Task<ActionResult<ResultOperationDTO<bool>>> Activate(string userId, bool isActive)
        {
            await _userService.ActivateUser(userId, isActive);
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<bool>.CreateSuccsessOperation(true, message: new string[] { "تمت عملية بنجاح" });
        }

        [HttpDelete(ManagmentControllerRouter.Delete)]
        public async Task<ActionResult<ResultOperationDTO<bool>>> DeleteUser(string userId, bool IsDelete)
        {

            var result = await _userService.DeleteUser(userId, IsDelete);
            await _unitOfWork.SaveChangeAsync();
            return ResultOperationDTO<bool>.CreateSuccsessOperation(result, message: new string[] { "تمت عملية بنجاح" });
        }
    }
}