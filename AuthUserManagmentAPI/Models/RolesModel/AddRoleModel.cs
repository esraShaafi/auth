﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthUserManagmentAPI.Models.RolesModel
{
    public class AddRoleModel
    {
        public string RoleName { get; set; }
        public long ModuleId { get; set; } = 0;
        public string RoleNameDisplay { get; set; }
        public bool? IsActive { get; set; }
        public string CreatedBy { get; set; }
        public List<string> PermissionIds { get; set; }
    }
}
