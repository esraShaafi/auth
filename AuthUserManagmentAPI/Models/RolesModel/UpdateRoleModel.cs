﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthUserManagmentAPI.Models.RolesModel
{
    public class UpdateRoleModel
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleNameDisplay { get; set; }
        public long ModuleID { get; set; } = 0;
        public List<string> PermissionIds { get; set; }
    }
}
