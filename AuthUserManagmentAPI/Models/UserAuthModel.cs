﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthUserManagmentAPI.Models
{
    public class UserAuthModel
    {
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
    }
}
