﻿using AutoMapper;
using Infra;
using Infra.Domain;
using Infra.DTOs;
using Infra.Repository;
using Infra.Services.IService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class RoleService : IRoleServices
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        
        }

        public async Task<bool> ActivateRole(string RoleId, bool isActive)
        {
            var r = await _unitOfWork.GetRepository<Roles>().GetAll();
            var result = await(await _unitOfWork.GetRepository<Roles>().
                FindBy(role => role.RoleId == RoleId)).SingleOrDefaultAsync();
            result.IsActive = !isActive;
            await _unitOfWork.GetRepository<Roles>().Update(result);
            return true;
        }

        public async Task<bool> DeleteRole(string RoleId, bool isDeleted)
        {
            var result = await (await _unitOfWork.GetRepository<Roles>()
                .FindBy(role => role.RoleId == RoleId)).SingleOrDefaultAsync();
            result.IsDeleted = !isDeleted;
            await _unitOfWork.GetRepository<Roles>().Delete(result);
            return true;
        }

        public async Task<IQueryable<Roles>> FindRoleBy(Expression<Func<Roles, bool>> predicate)
     => (await _unitOfWork.GetRepository<Roles>().FindBy(predicate));

        public async Task<List<RoleDTO>> GetActiveRoles(long moduleID, bool isActive)
        {
            //var result = await (await _unitOfWork.GetRepository<Roles>().FindBy(role => role.IsDeleted == false && role.IsActive == true &&role.ModuleId==moduleID)).Select(select => new RoleDTO
            //{
            //    RoleID = select.RoleId,
            //    RoleName = select.RoleName,
            //    RoleNameDisplay = select.RoleNameDisplay,


            //    RolePermissions = select.RolePermissions.Select(permission => new PermissionDTO
            //    {
            //        PermissionID = permission.Permission.PermissionId,
            //        PermissionDispaly = permission.Permission.PermissionDispaly
            //    }).ToList()
            //}).OrderByDescending(o => o.RoleID.ToList();
            //return result;

            var result = await (await _unitOfWork.GetRepository<Roles>().FindBy(role => role.ModuleId == moduleID && role.IsActive == isActive && role.IsDeleted == false)).Select(
           role => new RoleDTO
           {
               RoleID = role.RoleId,
               RoleName = role.RoleName,
               RoleNameDisplay = role.RoleNameDisplay,
               RolePermissions = role.RolePermissions.Select(permission => new PermissionDTO
               {
                   PermissionID = permission.Permission.PermissionId,
                   PermissionDispaly = permission.Permission.PermissionDispaly,
                   isActive = permission.Permission.IsActive,
                   PermissionName = permission.Permission.PermissionName

               }).ToList()
           }
            ).ToListAsync();
            return result;
        }

        public async Task<List<RoleDTO>> GetAllRoles(long moduleID, bool isDeleted)
        {
            var result = await(await _unitOfWork.GetRepository<Roles>().FindBy(role => role.ModuleId == moduleID  && role.IsDeleted == isDeleted)).Select(
        role => new RoleDTO
        {
            RoleID = role.RoleId,
            RoleName = role.RoleName,
            RoleNameDisplay = role.RoleNameDisplay,
            RolePermissions = role.RolePermissions.Select(permission => new PermissionDTO
            {
                PermissionID = permission.Permission.PermissionId,
                PermissionDispaly = permission.Permission.PermissionDispaly,
                isActive = permission.Permission.IsActive,
                PermissionName = permission.Permission.PermissionName

            }).ToList()
        }
         ).ToListAsync();
            return result;
        }

        public async Task<bool> InsertRole(Roles role)
        {
            await _unitOfWork.GetRepository<Roles>().Insert(role);
            return true;
        }

        public Task SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateRole(Roles entity)
        {
            await _unitOfWork.GetRepository<Roles>().Update(entity);
            return true;
        }
    }
}
