﻿using Infra;
using Infra.Domain;
using Infra.DTOs;
using Infra.Services.IService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class LocationServices : ILocationServices
    {

        private readonly IUnitOfWork _unitOfWork;
        public LocationServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<bool> ActivateLocation(string locationId, bool isActive)
        {
            var result = await(await _unitOfWork.GetRepository<Locations>().FindBy(location=>location.LocationId== locationId)).SingleOrDefaultAsync();
            result.IsActive = !isActive;

            await _unitOfWork.GetRepository<Locations>().Update(result);
            return true;
        }

        public async Task<bool> DeleteLocation(string locationId, bool isDelete)
        {
            var result = await (await _unitOfWork.GetRepository<Locations>().FindBy(location => location.LocationId == locationId)).SingleOrDefaultAsync();
            result.IsDelete = !isDelete;

            await _unitOfWork.GetRepository<Locations>().Delete(result);
            return true;
        }

        public async Task<IQueryable<Locations>> FindByLocation(Expression<Func<Locations, bool>> predicate)
        => await _unitOfWork.GetRepository<Locations>().FindBy(predicate);

        public async Task<List<LocationDTO>> GetActiveLocation()
        {
            var result =await(await _unitOfWork.GetRepository<Locations>().
                FindBy(location => location.IsDelete == false && location.IsActive == true)).Select(select=> new LocationDTO { 
                
                    IsActive=select.IsActive,
                    IsDelete=select.IsDelete,
                    CreateByUserId=select.CreateByUserId,
                    CreateByUserName=select.CreateByUserName,
                    CreateOn=select.CreateOn,
                    LocationId=select.LocationId,
                    LocationName=select.LocationName,
                    ModifyOn=select.ModifyOn
                
                }).ToListAsync();
            return result;
        }

        public async Task<List<LocationDTO>> GetLocation(string locationId, bool isDelete)
        {
            var result = await (await _unitOfWork.GetRepository<Locations>().
                 FindBy(location => location.IsDelete == isDelete && 
                 (string.IsNullOrEmpty(locationId)?true:location.LocationId== locationId)
                 )).Select(select => new LocationDTO
                 {

                     IsActive = select.IsActive,
                     IsDelete = select.IsDelete,
                     CreateByUserId = select.CreateByUserId,
                     CreateByUserName = select.CreateByUserName,
                     CreateOn = select.CreateOn,
                     LocationId = select.LocationId,
                     LocationName = select.LocationName,
                     ModifyOn = select.ModifyOn

                 }).ToListAsync();
            return result;
        }

        public async Task<bool> InsertLocation(Locations location)
        {
            await _unitOfWork.GetRepository<Locations>().Insert(location);
            return true;
        }

        public async Task<bool> UpdateLocation(Locations location)
        {
            await _unitOfWork.GetRepository<Locations>().Update(location);
            return true;
        }
    }
}
