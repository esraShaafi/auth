﻿using Infra;
using Infra.Utili;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class UserRoleServices:IUserRoleServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HelperUtili _helper;

        public UserRoleServices(IUnitOfWork unitOfWork, HelperUtili helper)
        {
            _unitOfWork = unitOfWork;
            _helper = helper;
        }

        public Task insertUserRole(string userId, List<string> roles)
        {
            throw new NotImplementedException();
        }

        public Task removeUserRole(string userId, long? moduleId)
        {
            throw new NotImplementedException();
        }
    }
}
