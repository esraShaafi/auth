﻿using Infra;
using Infra.Domain;
using Infra.DTOs;
using Infra.Services.IService;
using Infra.Utili;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class UserServices : IUserServices
    {
        private readonly IUnitOfWork _unitOfWork;
       
        private readonly HelperUtili _helper;

        public UserServices(IUnitOfWork unitOfWork, HelperUtili helper)
        {
            _unitOfWork = unitOfWork;
           _helper = helper;
        }

        public async Task<bool> ActivateUser(string userID, bool IsActive)
        {
            var result = await(await _unitOfWork.GetRepository<Users>().FindBy(user => user.UserId == userID)).SingleOrDefaultAsync();
            result.IsActive = !IsActive;
            await _unitOfWork.GetRepository<Users>().Update(result);
            return true;
        }

        public async Task<bool> DeleteUser(string userId, bool isDelete)
        {
          var result = await (await _unitOfWork.GetRepository<Users>().
              FindBy(user => user.UserId == userId)).SingleOrDefaultAsync();
            result.IsDelete = !isDelete;
            await _unitOfWork.GetRepository<Users>().Delete(result);
            return true;
        }

        public Task<bool> DeleteUser(string userId, Users entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Users>> FindUserBy(Expression<Func<Users, bool>> predicate)
         => (await _unitOfWork.GetRepository<Users>().FindBy(predicate));

        public Task<string[]> GetPermationUser(string userID)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetUserInModuleActive(string userId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<UsersDTO>> GetUsers()
        {
            var result = await(await _unitOfWork.GetRepository<Users>().FindBy
                (user => user.IsActive == true || user.IsActive == false)).Select(select => new UsersDTO
                {
                UserId = select.UserId,
                UserName = select.UserName,
                StateDescription = select.StateDescription,
                Email = select.Email
            }).ToListAsync();
            return result;
        }

        public Task<List<UsersWithRoleDTO>> GetUsersWithRoles(bool isPrimaryModule, UserClimesDTO currentUser, bool isActive)
        {
            throw new NotImplementedException();
        }

        public Task<List<UsersWithRoleDTO>> GetUsersWithRoles(UserClimesDTO currentUser, bool isActive)
        {
            throw new NotImplementedException();
        }

        public Task<List<UsersDTO>> GetUserWithDeActive(long moduleId, short userType)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> InsertUser(Users entity)
        {
            await _unitOfWork.GetRepository<Users>().Insert(entity);
            return true;
        }

        public Task InsertUserModule(string userID, long moduleID)
        {
            throw new NotImplementedException();
        }

        public Task IsActiveAndDeActiveUserModule(string userID, long moduleID, bool IsActive, bool isPrimaryModule)
        {
            throw new NotImplementedException();
        }

        public Task<bool> MoveUserInModule(string userID, long moduleId)
        {
            throw new NotImplementedException();
        }

        public Task<RestartPasswordDTO> RestartPassword(string userId)
        {
            throw new NotImplementedException();
        }

       

        public async Task<bool> UpdateUser(Users entity)
        {
            await _unitOfWork.GetRepository<Users>().Update(entity);
            return true;
        }

        public Task UpdateUserIsSendEmail(string userId)
        {
            throw new NotImplementedException();
        }
    }
}
