﻿using Infra;
using Infra.Domain;
using Infra.DTOs;
using Infra.Services.IService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class RolePermissionServices : IRolePermissionServices
    {
        private readonly IUnitOfWork _unitOfWork;

        public RolePermissionServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }
        //public async Task<List<PermissionDTO>> GetPermissionsInModule(long moduleId)
        //{
        //    var result = await(await _unitOfWork.GetRepository<Permissions>().
        //        FindBy(permission => permission.ModuleId == moduleId))
        //         .Select(selectPermission => new PermissionDTO
        //         {
        //             PermissionID = selectPermission.PermissionId,
        //             PermissionName = selectPermission.PermissionName
        //         }).ToListAsync();
        //    return result;
        //}

        public Task InsertRolePermission(string roleId, List<string> permissions)
        {
            throw new NotImplementedException();
        }

        public Task RemoveRolePermission(string roleId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<PermissionDTO>> GetPermissionsInModule(long moduleId)
        {
            var result = await(await _unitOfWork.GetRepository<Permissions>().
                FindBy(permission => permission.ModuleId == moduleId))
                 .Select(selectPermission => new PermissionDTO
                 {
                     PermissionID = selectPermission.PermissionId,
                     PermissionName = selectPermission.PermissionName,
                     PermissionDispaly=selectPermission.PermissionDispaly,
                     isActive=selectPermission.IsActive
                 }).ToListAsync();
            return result;
        }
    }
}
