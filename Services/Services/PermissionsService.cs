﻿using Infra;
using Infra.Domain;
using Infra.DTOs;
using Infra.Services.IService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
namespace Services.Services
{
    public class PermissionsService : IPermissionsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public PermissionsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IQueryable<Permissions>> FindPermissionBy(Expression<Func<Permissions, bool>> predicate)
       => (await _unitOfWork.GetRepository<Permissions>().FindBy(predicate));

        public async Task<bool> ActivatePermission(string PermissionId, bool isActive)
        {
            var r = await _unitOfWork.GetRepository<Permissions>().GetAll();
            var result = await (await _unitOfWork.GetRepository<Permissions>().
                FindBy(permission => permission.PermissionId == PermissionId)).SingleOrDefaultAsync();
            result.IsActive = !isActive;
            await _unitOfWork.GetRepository<Permissions>().Update(result);
            return true;
        }

        public async Task<List<PermissionDTO>> GetActivePermission()
        {
            var result = await (await _unitOfWork.GetRepository<Permissions>().FindBy(permission => permission.IsActive == true)).Select(select => new PermissionDTO
            {
                PermissionID = select.PermissionId,
                PermissionName = select.PermissionName,
                PermissionDispaly = select.PermissionDispaly,
                isActive = select.IsActive
            }).OrderByDescending(o => o.PermissionID).ToListAsync();
            return result;
        }

        public async Task<List<PermissionDTO>> getAllPermissions()
        {
            var result = await (await _unitOfWork.GetRepository<Permissions>().FindBy(permission => permission.IsActive == true || permission.IsActive == false)).Select(select => new PermissionDTO
            {
                PermissionID = select.PermissionId,
                PermissionName = select.PermissionName,
                PermissionDispaly = select.PermissionDispaly,
                isActive = select.IsActive
            }).ToListAsync();
            return result;
        }

        public Task<List<PermissionDTO>> GetPermissionIsArchiveOrDeArchive()
        {
            throw new NotImplementedException();
        }
    }
}
