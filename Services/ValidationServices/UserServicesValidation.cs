﻿using Infra.Services.IService;
using Infra.ValidationServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.ValidationServices
{
    public class UserServicesValidation : IUserServicesValidation
    {
        private readonly IUserServices _userServices;
        public UserServicesValidation(IUserServices userServices)
        {
            _userServices = userServices;
        }
        public async Task<bool> CheckEmail(string email)
          => await (await _userServices.FindUserBy(user => user.Email == email)).AnyAsync();

       
        public async Task<bool> CheckUserName(string userName)
        => await (await _userServices.FindUserBy(user => user.UserName == userName)).AnyAsync();

    }
}
