﻿using Infra.DTOs;
using Infra.Services.IService;
using Infra.ValidationServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ValidationServices
{
    public class LocationValidationServices : ILocationValidationServices
    {

        private readonly ILocationServices _locationServices;
        public LocationValidationServices(ILocationServices locationServices)
        {
            _locationServices = locationServices;
        }
        public async Task<bool> CheckIfLocationIsDeleted(string locationId)
        =>await (await _locationServices.FindByLocation(valid => valid.LocationId == locationId && valid.IsDelete == true)).AnyAsync();

        public async Task<bool> CheckIsExsistName(string locationName)
         => await (await _locationServices.FindByLocation(valid => valid.LocationName == locationName)).AnyAsync();

        public async Task<bool> CheckLocationInInnerJoin(string locationId)
          => await (await _locationServices.
            FindByLocation(valid => valid.InventoryLocations.Any(loc=>loc.LocationId== locationId &&
            (loc.Inventory.State==(short)StateInventory.IsCombleted && loc.Inventory.IsDelete==true) ))).AnyAsync();
    }
}
