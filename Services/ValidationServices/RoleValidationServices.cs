﻿using Infra.Services.IService;
using Infra.ValidationServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ValidationServices
{
    public class RoleValidationServices : IRoleValidationServices
    {
        IRoleServices _roleServices;
        public RoleValidationServices(IRoleServices roleServices)
        {
            _roleServices = roleServices;
        }

        public async Task<bool> CheckIfRoleIsDeleted(string RoleId)
          => await (await _roleServices.FindRoleBy(role => role.RoleId == RoleId && role.IsDeleted == true)).AnyAsync();

        public async Task<bool> CheckRoleInInnerJoin(string RoleId)
       => await (await _roleServices.FindRoleBy(role => role.Users.
       Any(user => user.IsDelete == false && user.RoleId == RoleId) )).AnyAsync();


        public async Task<bool> CheckRoleName(string roleName, long moduleId)
       => await (await _roleServices.FindRoleBy(role => role.RoleName == roleName && role.ModuleId == moduleId)).AnyAsync();

        public async Task<bool> CheckRoleNameDisplay(string roleNameDisplay, long moduleId)
     => await (await _roleServices.FindRoleBy(role => role.RoleNameDisplay == roleNameDisplay && role.ModuleId == moduleId)).AnyAsync();
       
    }
}
